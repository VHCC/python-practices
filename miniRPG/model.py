import random

from abc import ABCMeta, abstractmethod
from miniRPG.match import Match

class Character(metaclass = ABCMeta):
    @abstractmethod
    def attackedBy(self, attacker):
        pass

    def __init__(self, name, hp, strength, weapon = "fist"):
        self.__name = name   #private
        self._hp = hp       #protected
        self.__strength = strength  #private
        self.__weapon = weapon      #private

    def getName(self):
        return self.__name

    def isAlive(self):
        return (self._hp > 0)

    def _getPower(self):
        if(isinstance(self.__weapon, Weapon)):
            return self.__strength + self.__weapon.getStrength()
        return self.__strength

    def attack(self, target):
        if(isinstance(self.__weapon , Weapon)):
            weaponName = self.__weapon.getName()
        else:
            weaponName = self.__weapon

        print(self.__name + " attacks " + target.__name + " by " + weaponName)
        target._hp -= self._getPower()
        target.attackedBy(self)

class Weapon(object):
    def __init__(self, name: str, strength: int):
        self.__name = name
        self.__strength = strength

    def getName(self) -> str:
        return self.__name

    def getStrength(self):
        return self.__strength
        
class Hero(Character):
    def __init__(self, name, hp, strength, weapon):
        #Character.__init__(self, name, hp, strength, weapon)    #use super() instead
        super().__init__(name, hp, strength, weapon)    
        print(self.getName() + " is on the way to adventure.")

    def attackedBy(self, attacker):
        if(not self.isAlive()):
            print(self.getName() + " is dead.")
            return
        print(self.getName() + "'s hp remains %d" %self._hp)

    def burstInto(self, nest):
        for monster in nest:
            Match.begin(self, monster)
            if(not self.isAlive()):
                break

class Monster(Character):
    def attackedBy(self, attacker):
        if(not self.isAlive()):
            print(self.getName() + " is dead.")
        else:
            print(self.getName() + "'s hp decreased %d." %attacker._getPower())

#Iterator
class Nest(object):
    """
    Monster's nest
    """
    def __init__(self, *monsters):
        self.__monsters = list(monsters)            

    #return the object that implements __next__ function    
    def __iter__(self):
        return self

    #iterator must implement this function
    def __next__(self):
        try:
            m = random.choice(self.__monsters)
            self.__monsters.remove(m)
            return m
        except IndexError:
            raise StopIteration()